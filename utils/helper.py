
import datetime
import random

def generate_no_rekening():
    last_no_rekening = random.randint(0,9999)
    return str(last_no_rekening + 1)

def get_data_nasabah(data, nomor):
    for v in data['nasabah']:
        if nomor == v['no_rekening']:
            return v 
        else:
            continue
    return None

def get_nik_nasabah(data, nomor):
    for v in data['nasabah']:
        if nomor == v['nik']:
            return v 
        else:
            continue
    return None

def get_no_hp_nasabah(data, nomor):
    for v in data['nasabah']:
        if nomor == v['no_hp']: 
            return v
        else:
            continue
    return None

def generate_time():
    now = datetime.datetime.now()
    formatted_date = now.strftime("%Y-%m-%d %H:%M:%S")
    return formatted_date