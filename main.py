from fastapi import FastAPI, HTTPException
from pydantic import BaseModel

import utils.helper as helper
import json

app = FastAPI()

# Load data dari database.json
with open('database.json') as f:
    data = json.load(f)

class Nasabah(BaseModel):
    no_rekening: str
    nominal: int

class Registrasi(BaseModel):
    nama: str
    nik: str
    no_hp: str

class Mutasi(BaseModel):
    waktu: str
    kode_transaksi: str
    nominal: int

# Endpoint untuk registrasi
@app.post("/daftar")
def daftar(nasabah: Registrasi):
    nik_nasabah = helper.get_nik_nasabah(data, nasabah.nik)
    no_hp_nasabah = helper.get_no_hp_nasabah(data, nasabah.no_hp)
    
    # Cek apakah nik atau no_hp sudah digunakan
    if nik_nasabah is not None:
        raise HTTPException(status_code=400, detail="NIK sudah digunakan")
    if no_hp_nasabah is not None:
        raise HTTPException(status_code=400, detail="Nomor HP sudah digunakan")

    # Generate nomor rekening dan tambahkan data nasabah ke data
    no_rekening = helper.generate_no_rekening()
    data['nasabah'].append({
        "no_rekening": no_rekening,
        "nama": nasabah.nama,
        "nik": nasabah.nik,
        "no_hp": nasabah.no_hp,
        "saldo": 0,
        "mutasi": []
    })

    # Simpan perubahan ke file data
    with open("database.json", "w") as file:
        json.dump(data, file)

    return {"no_rekening": no_rekening}

# Endpoint untuk menabung saldo ke akun rekening
@app.post('/tabung')
def tabung_dana(nasabah: Nasabah):
    no_rekening = nasabah.no_rekening
    nominal = nasabah.nominal
    data_nasabah = helper.get_data_nasabah(data,no_rekening)
    if data_nasabah is None:
        raise HTTPException(status_code=400, detail="No. rekening tidak ditemukan")
    
    time = helper.generate_time()
    data_nasabah['saldo'] += nominal
    data_nasabah['mutasi'].append({'waktu': time, 'kode_transaksi': 'C', 'nominal': nominal})
    
    # Simpan perubahan ke file data
    with open("database.json", "w") as file:
        json.dump(data, file)
        
    return {'saldo': data_nasabah['saldo']}

# Endpoint untuk menarik saldo dari akun rekening
@app.post('/tarik')
def tarik_dana(nasabah: Nasabah):
    no_rekening = nasabah.no_rekening
    nominal = nasabah.nominal
    data_nasabah = helper.get_data_nasabah(data,no_rekening)
    if data_nasabah is None:
        raise HTTPException(status_code=400, detail="No. rekening tidak ditemukan")
    saldo = data_nasabah['saldo']
    if nominal > saldo:
        raise HTTPException(status_code=400, detail="Saldo tidak cukup")
    
    time = helper.generate_time()
    data_nasabah['saldo'] -= nominal
    data_nasabah['mutasi'].append({'waktu': time, 'kode_transaksi': 'D', 'nominal': nominal})
    
    # Simpan perubahan ke file data
    with open("database.json", "w") as file:
        json.dump(data, file)
        
    return {'saldo': data_nasabah['saldo']}

# Endpoint untuk melihat saldo
@app.get('/saldo/{no_rekening}')
def get_saldo(no_rekening: str):
    data_nasabah = helper.get_data_nasabah(data,no_rekening)
    if data_nasabah is None:
        raise HTTPException(status_code=400, detail="No. rekening tidak ditemukan")
    saldo = data_nasabah['saldo']
    return {'saldo': saldo}

# Endpoint untuk melihat mutasi
@app.get('/mutasi/{no_rekening}')
def get_mutasi(no_rekening: str):
    data_nasabah = helper.get_data_nasabah(data,no_rekening)
    if data_nasabah is None:
        raise HTTPException(status_code=400, detail="No. rekening tidak ditemukan")
    mutasi = []
    for transaksi in data_nasabah['mutasi']:
        mutasi.append(Mutasi(waktu=transaksi['waktu'], kode_transaksi=transaksi['kode_transaksi'], nominal=transaksi['nominal']))
    return {'mutasi': mutasi}
